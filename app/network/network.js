'use strict';

angular.module('myApp.network', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/network/:userId', {
            templateUrl: 'network/network.html',
            controller: 'NetworkCtrl'
        });
        $routeProvider.when('/network/suggestions/:userId', {
            templateUrl: 'network/suggestions.html',
            controller: 'NetworkSuggestionsCtrl'
        });
    }])

    .controller('NetworkCtrl', ['$routeParams', '$http', '$scope', function ($routeParams, $http, $scope) {
        var userId = $routeParams.userId;
        //$scope.

        //obtain User details:
        console.log("You are in user: " + userId);


        $http({
            method: 'GET',
            url: 'http://localhost:8080/user/' + userId
        }).then(function successCallback(response) {
            $scope.user = response.data;
            console.log("User Details:");
            console.log(response.data);
        }, function errorCallback(response) {
        });

        function refreshAllLists () {
            getFollowingList();
            getFollowersList();
            getFollowerRequests();
            getSuggestedList();
            getPendingFollowingList();
        }

        refreshAllLists();


        //Following List
        function getFollowingList() {
            $http({
                method: 'GET',
                url: 'http://localhost:8080/follow/following/' + userId
            }).then(function successCallback(response) {
                $scope.followingList = response.data;
                console.log("Following List");
                console.log(response.data);
            }, function errorCallback(response) {
            });
        }

        //Followers List
        function getFollowersList() {
            $http({
                method: 'GET',
                url: 'http://localhost:8080/follow/followers/' + userId
            }).then(function successCallback(response) {
                $scope.followersList = response.data;
                console.log("Followers List");
                console.log(response.data);
            }, function errorCallback(response) {
            });
        }

        //Accept Follower Request
        function getFollowerRequests() {
            $http({
                method: 'GET',
                url: 'http://localhost:8080/follow/accept/' + userId
            }).then(function successCallback(response) {
                $scope.acceptFollowerList = response.data;
                console.log("Yet to Accept Follower ");
                console.log(response.data);
            }, function errorCallback(response) {
            });
        }

        //Get user Suggestions to follow
        function getSuggestedList(){
            $http({
                method: 'GET',
                url: 'http://localhost:8080/follow/' + userId
            }).then(function successCallback(response) {
                $scope.suggestionUsers = response.data;
                console.log("All suggested Followers ");
                console.log(response.data);
            }, function errorCallback(response) {
            });

        }

        function getPendingFollowingList(){
            $http({
                method: 'GET',
                url: 'http://localhost:8080/follow/pending/' + userId
            }).then(function successCallback(response) {
                $scope.pendingRequestList = response.data;
                console.log("Yet to Accept Follower ");
                console.log(response.data);
            }, function errorCallback(response) {
            });
        }


        //Un-follow Following user
        $scope.unFollow = function (unFollowUser) {
            var toDeleteUser = {
                id: unFollowUser.id
            };
            console.log("In un-follow user");
            console.log(toDeleteUser);
            $http({
                method: 'DELETE',
                url: 'http://localhost:8080/follow/',
                data: toDeleteUser,
                headers: {
                    'Content-type': 'application/json;charset=utf-8'
                }
            }).then(function successCallback(response) {
                refreshAllLists();
                console.log("Deleted user.");
                console.log(response.data);
            }, function errorCallback(response) {
            });

        };

        $scope.follow = function (followSuggestedUser){
            var toFollowUser = {
                user : {
                    id : userId
                },
                followingUser : {
                    id : followSuggestedUser.id
                }
            };

            console.log("In Follow user");
            console.log(toFollowUser);
            $http({
                method: 'POST',
                url: 'http://localhost:8080/follow/',
                data: toFollowUser,
                headers: {
                    'Content-type': 'application/json;charset=utf-8'
                }
            }).then(function successCallback(response) {
                refreshAllLists();
                console.log("Following User user.");
                console.log(response.data);
            }, function errorCallback(response) {
            });
        };

        $scope.accept = function (acceptFollowRequest){
            var toAcceptUser = {
                id: acceptFollowRequest.id
            };
            console.log("In accept request");
            console.log(toAcceptUser);
            $http({
                method: 'POST',
                url: 'http://localhost:8080/follow/accept/',
                data: toAcceptUser,
                headers: {
                    'Content-type': 'application/json;charset=utf-8'
                }
            }).then(function successCallback(response) {
                refreshAllLists();
                console.log("Accepted user.");
                console.log(response.data);
            }, function errorCallback(response) {
            });
        }


    }]);