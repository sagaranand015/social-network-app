'use strict';

angular.module('myApp.user', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/user/:userId', {
            templateUrl: 'user/user.html',
            controller: 'UserCtrl'
        });
    }])

    .controller('UserCtrl', ['$routeParams', '$http', '$scope', function ($routeParams, $http, $scope) {
        var userId = $routeParams.userId;
        console.log("You are in user: " + userId);
        $http({
            method: 'GET',
            url: 'http://localhost:8080/user/' + userId
        }).then(function successCallback(response) {
            $scope.user = response.data;
            console.log(response.data);
        }, function errorCallback(response) {
        });


    }]);