'use strict';

angular.module('myApp.posts', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/posts/:userId', {
            templateUrl: 'posts/posts.html',
            controller: 'PostsCtrl'
        });
    }])

    .controller('PostsCtrl', ['$routeParams', '$http', '$scope', function ($routeParams, $http, $scope) {
        var userId = $routeParams.userId;
        console.log("You are in user: " + userId);
        $http({
            method: 'GET',
            url: 'http://localhost:8080/user/' + userId
        }).then(function successCallback(response) {
            $scope.user = response.data;
            console.log(response.data);
        }, function errorCallback(response) {
        });

        refreshAll();

        function refreshAll() {
            getAllPosts();
            getFeedOfUser();

        }


        function getAllPosts() {
            $http({
                method: 'GET',
                url: 'http://localhost:8080/post/' + userId
            }).then(function successCallback(response) {
                $scope.postList = response.data;
                console.log(response.data);
            }, function errorCallback(response) {
            });
        }

        function getFeedOfUser() {
            $http({
                method: 'GET',
                url: 'http://localhost:8080/post/feed/' + userId
            }).then(function successCallback(response) {
                $scope.feedList = response.data;
                console.log(response.data);
            }, function errorCallback(response) {
            });
        }

        $scope.likePost = function(post){
            var toSavePostLike = {
                user : {
                    id : Number(userId)
                },
                post : {
                    id : post.id
                }
            };

            console.log("About to like a post");
            console.log(toSavePostLike);

            $http({
                method: 'POST',
                url: 'http://localhost:8080/post/like',
                data: toSavePostLike,
                headers: {
                    'Content-type': 'application/json;charset=utf-8'
                }
            }).then(function successCallback(response) {
                refreshAll();
                //$scope.madePost = response.data;
                console.log(response.data);
            }, function errorCallback(response) {
            });

        };

        $scope.makeNewPost = function(){

            console.log("making a new Post");
            var toMakeNewPost = {
              message: $scope.newPost.message
            };

            console.log(toMakeNewPost);
            $http({
                method: 'POST',
                url: 'http://localhost:8080/post/' + userId,
                data: toMakeNewPost,
                headers: {
                    'Content-type': 'application/json;charset=utf-8'
                }
            }).then(function successCallback(response) {
                refreshAll();
                $scope.madePost = response.data;
                console.log(response.data);
            }, function errorCallback(response) {
            });

        }


    }]);